package solution

// Original system
//BEGIN_SCALA_BASE
trait Exp { def eval() : Int }
trait Lit extends Exp {
  val x:Int
  def eval() = x
}
trait Add extends Exp {
  val e1, e2 : Exp
  def eval() = e1.eval + e2.eval
}
//END_SCALA_BASE

// Adding Subtraction
//BEGIN_SCALA_SUB
trait Sub extends Exp {
  val e1, e2:Exp
  def eval() = e1.eval - e2.eval
}
//END_SCALA_SUB

// Adding printing
//BEGIN_SCALA_PRINT
trait ExpP extends Exp { def print():String}
trait LitP extends Lit with ExpP {
  def print() = "" + x
}
trait AddP extends Add with ExpP {
  val e1, e2 : ExpP  // type refined!
  def print() = "("+ e1.print + "+" + e2.print +")"
}
//END_SCALA_PRINT

trait SubP extends Sub with ExpP {
  val e1, e2 : ExpP
  def print() = "("+ e1.print + "-" + e2.print +")"
}

//BEGIN_SCALA_COLLECT
trait ExpC extends Exp { 
  def collectLit(): List[Int] 
}
trait LitC extends Lit with ExpC {
  def collectLit() : List[Int] = x :: List() 
}
trait AddC extends Add with ExpC {
  val e1, e2 : ExpC
  def collectLit() : List[Int] = e1.collectLit ::: e2.collectLit 
}
//END_SCALA_COLLECT

//BEGIN_SCALA_INDEPENDENT
trait ExpPC extends ExpP with ExpC
trait LitPC extends LitP with LitC with ExpPC
trait AddPC extends AddP with AddC with ExpPC {
  val e1, e2 : ExpPC
}
//END_SCALA_INDEPENDENT

object Test extends App {
  override def main(args : Array[String]) = {
    //BEGIN_SCALA_CLIENTCODE
    // Initial system
    val l1 = new Lit{val x=4}
    val l2 = new Lit{val x=3}
    println("l1.eval = " + l1.eval)
    val a = new Add{val e1=l1; val e2=l2}
    println("a.eval = " + a.eval)

    // Subtraction feature
    val s = new Sub{val e1=l1; val e2=l2}
    println("s.eval = " + s.eval)

    // Print feature
    val le1 = new LitP{val x=4}
    val le2 = new LitP{val x=3}
    val ae = new AddP{val e1=le1; val e2=le2}
    println(ae.print + " = " + ae.eval)
    
    // Independent extensibility
    val lpc1 = new LitPC{val x = 4}
    val lpc2 = new LitPC{val x = 6}
    val apc = new AddPC{val e1 = lpc1; val e2 = lpc2}
    println(apc.collectLit, apc.print, apc.eval)
    //END_SCALA_CLIENTCODE
    
    // Collect literals feature
    val lc1 = new LitC{val x = 4}
    val lc2 = new LitC{val x = 5}
    val ac = new AddC{val e1 = lc1; val e2 = lc2}
    val ac2 = new AddC{val e1 = ac; val e2 = lc2}
    println(ac2.collectLit)
    

  }
}