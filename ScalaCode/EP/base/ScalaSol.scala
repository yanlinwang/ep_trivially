package base


trait Exp { def eval() : Int }
class Lit(x : Int) extends Exp {
  def eval() = x
}
class Add(val e1 : Exp, val e2 : Exp) extends Exp {
  def eval() = e1.eval + e2.eval
}


class Sub(val e1 : Exp, val e2 : Exp) extends Exp {
  def eval() = e1.eval - e2.eval
}

trait ExpP extends Exp { def print() : String }
class LitP(x : Int) extends Lit(x) with ExpP {
  def print() = "" + x
}
class AddP(override val e1 : ExpP,
           override val e2 : ExpP)
  extends Add(e1, e2) with ExpP {
  def print() = "("+ e1.print + "+" + e2.print +")"
}

object Test extends App {
  override def main(args : Array[String]) = {
    // Initial system
    val l1 = new Lit(4)
    val l2 = new Lit(3)
    println("l1.eval = " + l1.eval)
    val a = new Add(l1, l2)
    println("a.eval = " + a.eval)

    // Subtraction feature
    val s = new Sub(l1, l2)
    println("s.eval = " + s.eval)

    // Print feature
    val le1 = new LitP(4)
    val le2 = new LitP(3)
    val ae = new AddP(le1, le2)
    println(ae.print + " = " + ae.eval)
  }
}