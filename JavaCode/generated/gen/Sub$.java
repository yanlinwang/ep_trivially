package gen;

import annoprocessingtest.Sub;

public class Sub$ extends Sub {
	annoprocessingtest.Exp e1;
	public annoprocessingtest.Exp e1() { return e1; }
	annoprocessingtest.Exp e2;
	public annoprocessingtest.Exp e2() { return e2; }
	public Sub$(annoprocessingtest.Exp e1, annoprocessingtest.Exp e2) {
		this.e1 = e1;
		this.e2 = e2;
	}
}