package gen;

import annoprocessingtest.Add;

public class Add$ extends Add {
	annoprocessingtest.Exp e1;
	public annoprocessingtest.Exp e1() { return e1; }
	annoprocessingtest.Exp e2;
	public annoprocessingtest.Exp e2() { return e2; }
	public Add$(annoprocessingtest.Exp e1, annoprocessingtest.Exp e2) {
		this.e1 = e1;
		this.e2 = e2;
	}
}