package gen;

import annoprocessingtest.AddP;

public class AddP$ extends AddP {
	annoprocessingtest.ExpP e1;
	public annoprocessingtest.ExpP e1() { return e1; }
	annoprocessingtest.ExpP e2;
	public annoprocessingtest.ExpP e2() { return e2; }
	public AddP$(annoprocessingtest.ExpP e1, annoprocessingtest.ExpP e2) {
		this.e1 = e1;
		this.e2 = e2;
	}
}