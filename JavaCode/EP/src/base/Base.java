package base;

//The expression problem!
//BEGIN_EXP_OO
interface Exp { int eval(); }
class Lit implements Exp {
    int x;  
    Lit(int x) { this.x = x; }
    public int eval() { return x; }
}
class Add implements Exp {
    Exp e1, e2;
    Add(Exp e1, Exp e2) {
        this.e1 = e1;
        this.e2 = e2;
    }
    public int eval() { 
        return e1.eval() + e2.eval(); 
    }
}
//END_EXP_OO

//support Subtraction
//BEGIN_SUB_OO
class Sub implements Exp {
    Exp e1, e2;
    Sub(Exp e1, Exp e2) { 
        this.e1 = e1; 
        this.e1 = e2; 
    }
    public int eval() { 
        return e1.eval() - e2.eval(); 
    }
}
//END_SUB_OO

//support Printing
//BEGIN_PRINT_OO
interface ExpP extends Exp { String print(); }
class LitP extends Lit implements ExpP {
    LitP(int x) { super(x); }
    public String print() { return "" + this.x; }
}
class AddP extends Add implements ExpP {
    AddP(Exp e1, Exp e2) { 
        super(e1,e2); 
    }
    public String print() {
        return    ((ExpP) e1).print() 
                + " + " 
                + ((ExpP) e2).print();
    }
}
//END_PRINT_OO

public class Base {

    public static void main(String[] args) {
        //BEGIN_PRINT_OO_CLIENT
        ExpP p = new AddP(new LitP(4), new LitP(9));
        System.out.println(p.print() + " = " + p.eval());
        //END_PRINT_OO_CLIENT

        //BEGIN_CAST_PROBLEM
        ExpP a = new AddP(new Lit(4), new Lit(9));
        // System.out.println(a.print());
        // The above line (if not commented out) would cause a run-time error.

        AddP a2 = new AddP(new LitP(1), new LitP(2));
        System.out.println(a2.print());
        a2.e1 = new Lit(3);
        // System.out.println(a.print());
        // The above line (if not commented out) would cause a run-time error.
        //END_CAST_PROBLEM
    }
}