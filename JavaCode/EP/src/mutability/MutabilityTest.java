package mutability;

interface Exp { int eval(); }
class Lit implements Exp {
    int x;
    Lit(int x) { this.x = x; }
    public int eval() { return x; }
}
interface ExpP extends Exp { String print(); }
class LitP extends Lit implements ExpP {
    LitP(int x) { super(x); }
    public String print() { return "" + x; }
}

//BEGIN_MUTABILITY
class Add<E extends Exp> implements Exp {
    E e1, e2;
    public int eval() {return e1.eval() + e2.eval();}
}
class AddP<E extends ExpP> extends Add<E> implements ExpP {
    public String print() {
        return e1.print() + " + " + e2.print();
    }
}
//END_MUTABILITY
public class MutabilityTest {
    public static void main(String[] args) {
        AddP ap = new AddP();
        ap.e1 = new LitP(3);
        ap.e2 = new LitP(4);
        System.out.println(ap.print() + " = " + ap.eval());
    }
}
