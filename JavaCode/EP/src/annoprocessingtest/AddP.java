package annoprocessingtest;

import annotation.GenImpl;

@GenImpl
public abstract class AddP extends Add implements ExpP {
    public abstract ExpP e1(); // return type refined!

    public abstract ExpP e2(); // return type refined!

    public String print() {
        return "(" + e1().print() + " + " + e2().print() + ")";
    }
}
