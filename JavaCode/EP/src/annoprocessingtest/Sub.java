package annoprocessingtest;

import annotation.GenImpl;

@GenImpl
public abstract class Sub {
    public abstract Exp e1();

    public abstract Exp e2();

    int eval() {
        return e1().eval() - e2().eval();
    }
}
