package annoprocessingtest;

import gen.Add$;
import gen.AddP$;

class Lit implements Exp {
    int x;

    Lit(int x) {
        this.x = x;
    }

    public int eval() {
        return x;
    }
}

class LitP extends Lit implements ExpP {
    LitP(int x) {
        super(x);
    }

    public String print() {
        return "" + x;
    }
}



public class Test {
    public static void main(String[] args) {
        Add e1 = new Add$(new Lit(3), new Lit(4));
        System.out.println(e1.eval());

        AddP e2 = new AddP$(new LitP(7), new LitP(4));
        System.out.println(e2.print());
    }
}
