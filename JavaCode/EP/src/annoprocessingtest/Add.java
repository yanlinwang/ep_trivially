package annoprocessingtest;

import annotation.GenImpl;

@GenImpl 
public abstract class Add implements Exp {
    public abstract Exp e1(); // refinable return type!

    public abstract Exp e2(); // refinable return type!

    public int eval() {
        return e1().eval() + e2().eval();
    }
}
