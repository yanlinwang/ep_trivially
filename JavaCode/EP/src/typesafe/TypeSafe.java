package typesafe;

//BEGIN_REMOVE_CAST_INITIAL_CODE
interface Exp { int eval(); }
class Lit implements Exp {
    int x;
    Lit(int x) { this.x = x; }
    public int eval() { return x; }
}
abstract class Add implements Exp {
    abstract Exp getE1(); //refinable return type!
    abstract Exp getE2(); //refinable return type!
    public int eval() { 
        return getE1().eval() + getE2().eval(); 
    }
}
//END_REMOVE_CAST_INITIAL_CODE

//BEGIN_ADDFINAL
class AddFinal extends Add {
    Exp e1, e2;
    AddFinal(Exp e1, Exp e2) { 
        this.e1 = e1;
        this.e2 = e2; 
    }
    Exp getE1() { return e1; }
    Exp getE2() { return e2; }
}
//END_ADDFINAL

//support subtraction
//BEGIN_REMOVE_CAST_SUB
abstract class Sub implements Exp {
    abstract Exp getE1();
    abstract Exp getE2();
    public int eval() { 
        return getE1().eval() - getE2().eval(); 
    }
}
//END_REMOVE_CAST_SUB

class SubFinal extends Sub {
    Exp e1, e2;
    SubFinal(Exp e1, Exp e2) { 
        this.e1 = e1; 
        this.e2 = e2; 
    }
    Exp getE1() { return e1; }
    Exp getE2() { return e2; }
}

//support print operation
//BEGIN_REMOVE_CAST_PRINT
interface ExpP extends Exp { String print(); }
class LitP extends Lit implements ExpP {
    LitP(int x) { super(x); }
    public String print() { return "" + x; }
}
abstract class AddP extends Add implements ExpP {
    abstract ExpP getE1(); //return type refined!
    abstract ExpP getE2(); //return type refined!
    public String print() {
        return  "(" + getE1().print() + " + " + 
                getE2().print() + ")";
    }
}
//END_REMOVE_CAST_PRINT

class AddPFinal extends AddP {
    ExpP e1, e2;
    AddPFinal(ExpP e1, ExpP e2) {
        this.e1 = e1;
        this.e2 = e2;
    }
    ExpP getE1() { return e1; }
    ExpP getE2() { return e2; }
    void setE1(ExpP e1) { this.e1 = e1; }
    void setE2(ExpP e2) { this.e2 = e2; }
}


public class TypeSafe {
    public static void main(String args[]) {
        System.out.println("======Add======");
        Add add = new AddFinal(new Lit(7), new Lit(4));
        System.out.println(add.eval());
        
        System.out.println("======Sub======");
        Sub sub = new SubFinal(new Lit(7), new Lit(4));
        System.out.println(sub.eval());

        System.out.println("======Print======");
        /* the line below causes compile-time error, if now commented out. */
        // AddPFinal exp = new AddPFinal(new Lit(7)), new Lit(4));
        AddPFinal exp = new AddPFinal(new LitP(7), new LitP(4));
        System.out.println(exp.print() + " = " + exp.eval());
    }

}
